package com.example.login_signup_with_pattern.network

import com.example.login_signup_with_pattern.model.LoginModel
import com.example.login_signup_with_pattern.model.SignUpModel
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface ApiInterface {

    @POST("api/login")
    @FormUrlEncoded
    fun callLogin(@Field("username")username : String,
                  @Field("password") password : String
    ):Call<LoginModel>

    @POST("api/signUp")
    @FormUrlEncoded
    fun callSignup(@Field("username") username : String,
                   @Field("email") email : String,
                   @Field("password") password : String
    ):Call<SignUpModel>
}