package com.example.login_signup_with_pattern.network

import retrofit2.Response

interface ApiResponse {
    fun onResponse(response : Response<Any>)
    fun onFailure(msg : String)
}