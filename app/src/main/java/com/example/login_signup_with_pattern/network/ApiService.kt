package com.example.login_signup_with_pattern.network

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ApiService<T> {

    fun getResponse(apiResponse: ApiResponse, mCall : Call<Any>){
        mCall.enqueue(object : Callback<Any>{
            override fun onFailure(call: Call<Any>, t: Throwable) {
                apiResponse.onFailure(t.toString())
            }

            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                apiResponse.onResponse(response)
            }

        })

    }
}