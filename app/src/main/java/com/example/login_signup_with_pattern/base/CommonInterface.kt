package com.example.login_signup_with_pattern.base

import android.content.Context
import android.content.Intent
import androidx.core.content.ContextCompat.startActivity

interface CommonInterface {
    fun onError(key: String?)
    fun intent(context: Context,className:Class<Any>){
        val inten = Intent(context,className::class.java)
        startActivity(context,inten,null)

    }
}