package com.example.login_signup_with_pattern.presenter

import com.example.login_signup_with_pattern.base.CommonInterface
import com.example.login_signup_with_pattern.network.ApiInterface
import kotlinx.android.synthetic.main.activity_login.*

class LoginPrsenter(val interfac : CommonInterface) {

    val apiInterface : ApiInterface ?= null

    fun login(username : String
              ,password : String)
    {
        apiInterface!!.callLogin(username,password)
    }
}