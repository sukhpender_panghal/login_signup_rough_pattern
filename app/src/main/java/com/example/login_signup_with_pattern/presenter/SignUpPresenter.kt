package com.example.login_signup_with_pattern.presenter

import com.example.login_signup_with_pattern.base.CommonInterface
import com.example.login_signup_with_pattern.network.ApiInterface

class SignUpPresenter(interfac : CommonInterface) {

    val apiInterface : ApiInterface? = null

    fun signUp(
        username: String,
        email: String,
        password: String
    ){
        apiInterface!!.callSignup(username, email, password)
    }

}