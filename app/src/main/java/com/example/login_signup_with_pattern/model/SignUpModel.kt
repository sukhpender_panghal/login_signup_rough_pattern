package com.example.login_signup_with_pattern.model

import com.google.gson.annotations.SerializedName

class SignUpModel (
    @SerializedName("username") val username : String?,
    @SerializedName("email") val email : String?,
    @SerializedName("password") val password : String?
)