package com.example.login_signup_with_pattern.model

import com.google.gson.annotations.SerializedName

class LoginModel(
    @SerializedName("username") val username : String?,
    @SerializedName("password") val password : String?
)