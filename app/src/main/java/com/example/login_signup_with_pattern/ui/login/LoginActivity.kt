package com.example.login_signup_with_pattern.ui.login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.login_signup_with_pattern.R
import com.example.login_signup_with_pattern.base.CommonInterface
import com.example.login_signup_with_pattern.presenter.LoginPrsenter
import com.example.login_signup_with_pattern.ui.signUp.SignUpActivity
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity(), CommonInterface {

    val loginPresenter = LoginPrsenter(this)
    val cmn : CommonInterface ?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        onClick()
    }

    private fun onClick() {
        btn_sign_in.setOnClickListener {
            if (edt_username == null || edt_password == null) {
                cmn?.onError("please fill the details")

            } else {
                loginPresenter.login(edt_username.text.toString(), edt_password.text.toString())
            }
        }
        txt_sign_up.setOnClickListener {
            val i = Intent(this, SignUpActivity::class.java)
            startActivity(i)
        }
    }

    override fun onError(key: String?) {
        Toast.makeText(this,key, Toast.LENGTH_SHORT).show()
    }
}