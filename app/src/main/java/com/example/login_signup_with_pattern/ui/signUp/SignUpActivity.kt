package com.example.login_signup_with_pattern.ui.signUp

import android.content.Intent
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.example.login_signup_with_pattern.R
import com.example.login_signup_with_pattern.base.CommonInterface
import com.example.login_signup_with_pattern.presenter.SignUpPresenter
import com.example.login_signup_with_pattern.ui.login.LoginActivity
import kotlinx.android.synthetic.main.activity_sign_up.*

class SignUpActivity : AppCompatActivity(), CommonInterface {

    private val signUpPresenter = SignUpPresenter(this)
    val cmn: CommonInterface? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        onClick()
    }

    private fun openGallery() {
        val gallery = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI)
        startActivityForResult(gallery, 100)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == RESULT_OK && requestCode == 100) {
            val imageUri = data!!.data
            Log.e("path1","$imageUri")
            //imgProfile.setImageURI(imageUri)
            Glide.with(this)
                .load(imageUri)
                .into(imgProfile)
        } else {
            cmn!!.onError("Task Cancelled")
        }
    }

    private fun onClick() {

        btn_sign_up.setOnClickListener {
            signUpPresenter.signUp(
                edt_username_signup.text.toString(),
                edt_mail_id_signup.text.toString(),
                edt_password_signup.text.toString()
            )
        }

        imgProfile.setOnClickListener {
            openGallery()
        }

        txt_sign_in.setOnClickListener {
            val i = Intent(this, LoginActivity::class.java)
            startActivity(i)
        }
    }

    override fun onError(key: String?) {
        Toast.makeText(this, key, Toast.LENGTH_SHORT).show()
    }
}